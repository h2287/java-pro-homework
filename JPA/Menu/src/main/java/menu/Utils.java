package menu;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class Utils {

    public static void addDish() {
        String title;
        int weight;
        float price;
        Scanner sc = new Scanner(System.in);
        System.out.print("Додати нову страву\n");
        System.out.print("1. Введіть назву: ");
        title = String.valueOf(sc.nextLine());
        System.out.print("Введіть вагу: ");
        weight = Integer.parseInt(sc.nextLine());
        System.out.print("Введіть ціну: ");
        price = Float.parseFloat(sc.nextLine());
        System.out.print(price);
        BigDecimal convAmount = new BigDecimal(price);
        System.out.print(convAmount);
        BigDecimal convertedAmtTwoDig = convAmount.setScale(2, RoundingMode.HALF_UP);
        price = convertedAmtTwoDig.floatValue();
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("menu");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            Dish newDish = new Dish(title, weight, price);
            em.persist(newDish);
            em.getTransaction().commit();
            System.out.println("Dish added: " + newDish);

        } catch (Exception ex) {
            em.getTransaction().rollback();
            System.out.printf("Error!");

        } finally {
            em.close();
            emf.close();
        }
    }

    public static void addDiscount() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter dish id: ");
        Long dishId = Long.valueOf(sc.nextLine());
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("menu");
        EntityManager em = emf.createEntityManager();
        Dish a = em.getReference(Dish.class, dishId);
        if (a == null) {
            System.out.println("Dish not found!");
            return;
        }
        System.out.print("Enter discount to be added for dish: " + a.getDish());
        int discount = Integer.valueOf(sc.nextLine());
        a.setDiscount(discount);
        em.getTransaction().begin();
        try {
            em.persist(a);
            em.getTransaction().commit();
            System.out.println("Discount added!");


        } catch (Exception ex) {
            System.out.printf("Error!");

        } finally {
            em.close();
            emf.close();
        }


    }

    public static void deleteDish() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter dish id: ");
        Long dishId = Long.valueOf(sc.nextLine());
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("menu");
        EntityManager em = emf.createEntityManager();
        Dish a = em.getReference(Dish.class, dishId);
        if (a == null) {
            System.out.println("Dish not found!");
            return;
        }
        em.getTransaction().begin();
        try {
            em.refresh(a);
            em.getTransaction().commit();
            System.out.println("Dish deleted" + a);


        } catch (Exception ex) {
            System.out.printf("Error!");

        } finally {
            em.close();
            emf.close();
        }


    }

    public static List<Dish> findDishes() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter min price\n");
        float minPrice = Float.parseFloat(sc.nextLine());
        System.out.println("Enter max price:\n");
        float maxPrice = Float.parseFloat(sc.nextLine());
        System.out.println(minPrice);
        System.out.println(maxPrice);
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("menu");
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("from Dish a where a.price>=:minPrice AND a.price<=:maxPrice")
                .setParameter("minPrice", minPrice)
                .setParameter("maxPrice", maxPrice);
        List<Dish> list = query.getResultList();
        if (list.isEmpty()) {
            System.out.println("NO dishes found!\n");

        } else {
            System.out.println("We FOUND the following dishes:\n");
            System.out.println("Dish\t ID\tMass\tPrice\tDiscount\t");
            for (Dish a : list)
                System.out.println(a.getDish() + "\t" + a.getId() + "\t" + a.getMass() + "\t" + a.getPrice() + "\t" + a.getDiscount());
        }
        em.close();
        emf.close();
        return list;

    }

    public static List<Dish> findAllDiscounts() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("menu");
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("from Dish a where a.discount>:discount") //так можна шукати і за розміром знижки
                .setParameter("discount", 0);
        List<Dish> list = (List<Dish>) query.getResultList();
        if (list.isEmpty()) {
            System.out.println("NO dishes found!\n");
        } else {
            System.out.printf("We FOUND the following dishes:\n");
            System.out.println("Dish\tID\tMass\tPrice\tDiscount\t");
            for (Dish a : list)
                System.out.println(a.getDish() + "\t" + a.getId() + "\t" + a.getMass() + "\t" + a.getPrice() + "\t" + a.getDiscount());
        }
        em.close();
        emf.close();
        return list;
    }

    public static void findOneKilo() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("menu");
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("from Dish a");
        List<Dish> list = (List<Dish>) query.getResultList();
        Random rand = new Random();
        Set<Dish> oneKiloList = new HashSet<>();
        int weight = 0;
        while (weight <= 1000) {
            Dish randomDish = list.get(rand.nextInt(list.size()));
            if ((weight + randomDish.getMass()) < 1000) {
                oneKiloList.add(randomDish);

            }
            weight += randomDish.getMass();
        }
        weight = 0;
        for (Dish a : oneKiloList) {
            weight += a.getMass();
        }
        System.out.println(weight + "current weight");
        for (Dish d : list) {
            if (!oneKiloList.contains(d) && d.getMass() < (1000 - weight)) {
                oneKiloList.add(d);
                weight += d.getMass();
            }
        }
        System.out.println("The menu we found for you up to 1 kilo:");
        System.out.println("Dish\t ID\tMass\tPrice\tDiscount\t");
        for (Dish a : oneKiloList)
            System.out.println(a.getDish() + "\t" + a.getId() + "\t" + a.getMass() + "\t" + a.getPrice() + "\t" + a.getDiscount());
        System.out.println("Total weight: " + weight + "g.");
    }
}
