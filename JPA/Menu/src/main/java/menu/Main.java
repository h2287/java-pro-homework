package menu;

import java.util.Scanner;

import static menu.Utils.*;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("""
                    Select:
                    1 - add a new Dish;
                    2 - find a dish by price;
                    3 - show all dishes with discount;
                    4 - add discount to a dish;
                    5 - delete a dish;
                    6 - find 1 kg of dishes
                    """);
            String choice = sc.nextLine();
            switch (choice) {
                case "1":
                    addDish();
                    break;
                case "2":
                    findDishes();
                    break;
                case "3":
                    findAllDiscounts();
                    break;
                case "4":
                    addDiscount();
                    break;
                case "5":
                    deleteDish();
                    break;
                case "6":
                    findOneKilo();
                    break;
                default:
                    return;

            }
        }
    }
}
