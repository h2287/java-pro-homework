package bank;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class Utils {
    static EntityManagerFactory emf;

    static EntityManager em;

    public static Client addClient() {
        emf = Persistence.createEntityManagerFactory("bank");
        em = emf.createEntityManager();
        System.out.println("\tYou're about to add a new client\n");
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Name: ");
        String newName = sc.nextLine();
        System.out.print("Enter Surname: ");
        String newSurname = sc.nextLine();

        Client newClient = new Client(newName, newSurname);

        em.getTransaction().begin();
        try {
            em.persist(newClient);
            em.getTransaction().commit();

            System.out.println("\tNew client added: " + newClient);

        } catch (Exception ex) {
            em.getTransaction().rollback();
            System.out.println("Error!");
        } finally {
            em.close();
            emf.close();
        }
        return newClient;
    }

    public static Client findClient(Long id) {
        emf = Persistence.createEntityManagerFactory("bank");
        em = emf.createEntityManager();
        Query query = em.createQuery("from Client c where c.id=:id")
                .setParameter("id", id);
        Client foundClient = (Client) query.getSingleResult();
        return foundClient;
    }

    private static void addRate() {
        emf = Persistence.createEntityManagerFactory("bank");
        em = emf.createEntityManager();
        UahRate newRate = new UahRate();
        em.getTransaction().begin();
        try {
            em.persist(newRate);
            em.getTransaction().commit();
            System.out.println("new exchange rate today!");
            System.out.println("UAH/USD = " + newRate.getUsdRate());
            System.out.println("UAH/EUR = " + newRate.getEurRate());
        } catch (Exception ex) {
            em.getTransaction().rollback();
            System.out.println("Error!");
        } finally {
            em.close();
            emf.close();
        }
    }


    public static void addAccount() {
        EntityManagerFactory emf2 = Persistence.createEntityManagerFactory("bank");
        EntityManager em2 = emf2.createEntityManager();
        Account newAcc;
        Scanner sc = new Scanner(System.in);
        System.out.println("\tEnter your bank ID");
        Long clientID = Long.valueOf(sc.nextLine());
        em2.getTransaction().begin();
        try {
            Client client = findClient(clientID);
            System.out.println("Found a client: " + client);
            System.out.println("Enter currency (UAH, USD or EUR)");
            String currencyIn = sc.nextLine().toUpperCase();
            switch (currencyIn) {
                case "UAH":
                    currencyIn = Currencies.UAH.name();
                    System.out.println(currencyIn);
                    break;
                case "USD":
                    currencyIn = Currencies.USD.name();
                    break;
                case "EUR":
                    currencyIn = Currencies.EUR.name();
                    break;
                default:
                    System.out.println("Wrong input!");
                    return;
            }
            newAcc = new Account(currencyIn);
            System.out.println("creating new account");
            newAcc.setClient(client);
            em2.merge(client);
            em2.persist(newAcc);
            System.out.println("saving...");
            em2.getTransaction().commit();
            if (newAcc != null) {
                System.out.println("\tAccount created: " + newAcc.toString());
            }


        } catch (Exception e) {
            em2.getTransaction().rollback();
            throw new RuntimeException(e);
        } finally {
            em2.close();
            emf2.close();
        }
    }

    public static void getMyAccount(Long id) {
        Client client = findClient(id);
        System.out.println(client);
    }

    public static void deposit() {
        double amount;
        Account depositedAccount;
        EntityManagerFactory emf2 = Persistence.createEntityManagerFactory("bank");
        EntityManager em2 = emf2.createEntityManager();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your bank account number");
        Long accNumber = Long.valueOf(sc.nextLine());
        System.out.println("Enter amount to deposit");
        amount = Double.valueOf(sc.nextLine());
        try {
            Query query = em2.createQuery("from Account a where a.acc_Number=:acc_n")
                    .setParameter("acc_n", accNumber);
            depositedAccount = (Account) query.getSingleResult();
            System.out.println("Depositing into account No. " + depositedAccount.getAcc_Number() + depositedAccount.getAcc_Currency());
            em2.getTransaction().begin();
            em2.lock(depositedAccount, LockModeType.PESSIMISTIC_WRITE);
            depositedAccount.setBalance(depositedAccount.getBalance() + amount);
            em2.persist(depositedAccount);
            em2.lock(depositedAccount, LockModeType.NONE);
            em2.getTransaction().commit();
            System.out.println("\tDeposit successful: ");
            System.out.println("\tYour balance is: " + depositedAccount.getBalance() + depositedAccount.getAcc_Currency());


        } catch (Exception e) {
            System.out.println("Error");
        } finally {
            em2.close();
            emf2.close();
        }
    }

    public static boolean deposit(long acc, double amount) {
        boolean result = false;
        Account depositedAccount;
        EntityManagerFactory emf2 = Persistence.createEntityManagerFactory("bank");
        EntityManager em2 = emf2.createEntityManager();
        try {
            Query query = em2.createQuery("from Account a where a.acc_Number=:acc_n")
                    .setParameter("acc_n", acc);
            depositedAccount = (Account) query.getSingleResult();
            em2.getTransaction().begin();
            em2.lock(depositedAccount, LockModeType.PESSIMISTIC_WRITE);
            depositedAccount.setBalance(depositedAccount.getBalance() + amount);
            em2.persist(depositedAccount);
            em2.lock(depositedAccount, LockModeType.NONE);
            em2.getTransaction().commit();
            result = true;
        } catch (Exception e) {
            System.out.println("Error");
        } finally {
            em2.close();
            emf2.close();
        }
        return result;
    }

    public static void withdraw() {
        double amount;
        Account withdrawalAccount;
        EntityManagerFactory emf2 = Persistence.createEntityManagerFactory("bank");
        EntityManager em2 = emf2.createEntityManager();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your bank account number");
        Long accNumber = Long.valueOf(sc.nextLine());
        System.out.println("Enter amount to withdraw");
        amount = Double.valueOf(sc.nextLine());
        try {
            Query query = em2.createQuery("from Account a where a.acc_Number=:acc_n")
                    .setParameter("acc_n", accNumber);
            withdrawalAccount = (Account) query.getSingleResult();
            System.out.println("Withdrawing from account No. " +
                    withdrawalAccount.getAcc_Number() +
                    withdrawalAccount.getAcc_Currency());
            em2.getTransaction().begin();
            em2.lock(withdrawalAccount, LockModeType.PESSIMISTIC_WRITE);
            if (withdrawalAccount.getBalance() >= amount) {
                withdrawalAccount.setBalance(withdrawalAccount.getBalance() - amount);
                em2.persist(withdrawalAccount);
                em2.lock(withdrawalAccount, LockModeType.NONE);
                em2.getTransaction().commit();
                System.out.println("\tWithdrawal successful: ");
                System.out.println("\tYour balance is: " + withdrawalAccount.getBalance() + withdrawalAccount.getAcc_Currency());

            } else {
                System.out.println("Insufficient funds! Your balance is: " + withdrawalAccount.getBalance() + withdrawalAccount.getAcc_Currency());
            }
        } catch (Exception e) {
            System.out.println("Error");
        } finally {
            em2.close();
            emf2.close();
        }
    }

    public static boolean withdraw(long acc, double amount) {
        boolean result = false;
        EntityManagerFactory emf2 = Persistence.createEntityManagerFactory("bank");
        EntityManager em2 = emf2.createEntityManager();
        try {
            Query query = em2.createQuery("from Account a where a.acc_Number=:acc_n")
                    .setParameter("acc_n", acc);
            Account wihtdrawAccount = (Account) query.getSingleResult();
            em2.getTransaction().begin();
            em2.lock(wihtdrawAccount, LockModeType.PESSIMISTIC_WRITE);
            if (wihtdrawAccount.getBalance() >= amount) {
                wihtdrawAccount.setBalance(wihtdrawAccount.getBalance() - amount);
                em2.persist(wihtdrawAccount);
                em2.lock(wihtdrawAccount, LockModeType.NONE);
                em2.getTransaction().commit();
                result = true;
            } else {
                System.out.println("Insufficient funds! Your balance is: " +
                        wihtdrawAccount.getBalance() +
                        wihtdrawAccount.getAcc_Currency());
            }
        } catch (Exception e) {
            System.out.println("Error");
        } finally {
            em2.close();
            emf2.close();
        }
        return result;
    }

    public static void transfer() {
        double amount;
        Account wihtdrawAccount;
        Account depositAccount;

        EntityManagerFactory emf2 = Persistence.createEntityManagerFactory("bank");
        EntityManager em2 = emf2.createEntityManager();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter source account number (from):");
        Long withrAcc = Long.valueOf(sc.nextLine());
        System.out.println("Enter destination account number (to):");
        Long depAcc = Long.valueOf(sc.nextLine());
        try {
            Query query = em2.createQuery("from Account a where a.acc_Number=:acc_n")
                    .setParameter("acc_n", withrAcc);
            wihtdrawAccount = (Account) query.getSingleResult();
            System.out.println(wihtdrawAccount);
            query = em2.createQuery("from Account a where a.acc_Number=:acc_n")
                    .setParameter("acc_n", depAcc);
            depositAccount = (Account) query.getSingleResult();

            if (!depositAccount.getAcc_Currency().equals(wihtdrawAccount.getAcc_Currency()) || withrAcc == depAcc || wihtdrawAccount.getBalance() == 0) {
                System.out.println("\tOperation not possible! ");
            } else {
                System.out.println("\tWithdrawing from account No. " +
                        wihtdrawAccount.getAcc_Number() +
                        wihtdrawAccount.getAcc_Currency());
                System.out.println("Enter amount to withdraw");
                amount = Double.valueOf(sc.nextLine());
                boolean wResult = withdraw(withrAcc, amount);
                if (wResult) {

                    boolean finalResult = deposit(depAcc, amount);
                    if (finalResult) {
                        EntityManagerFactory emfAfter = Persistence.createEntityManagerFactory("bank");
                        EntityManager emAfter = emfAfter.createEntityManager();
                        Query queryAfter = emAfter.createQuery("from Account a where a.acc_Number=:acc_n")
                                .setParameter("acc_n", withrAcc);
                        Account withdrawalAccountUpdated = (Account) queryAfter.getSingleResult();
                        queryAfter = emAfter.createQuery("from Account a where a.acc_Number=:acc_n")
                                .setParameter("acc_n", depAcc);
                        Account depositAccountUpdated = (Account) queryAfter.getSingleResult();
                        System.out.println("\tTransfer successful!");
                        System.out.println("\tAccount " + withdrawalAccountUpdated.getAcc_Number() +
                                " remaining balance is: " + withdrawalAccountUpdated.getBalance());
                        System.out.println("\tAccount " + depositAccountUpdated.getAcc_Number() +
                                " remaining balance is: " + depositAccountUpdated.getBalance());
                    }
                } else {
                    System.out.println("Something went wrong!");
                }
            }
        } catch (Exception e) {
            System.out.println("Error!");

        }
    }

    public static void exchange() {
        Double toBeConvertedAmount;
        Double convertedAmount;
        Client client;
        Set<Account> accList;
        Query query = em.createQuery("from UahRate r where r.date=:date")
                .setParameter("date", LocalDate.now());
        List<UahRate> rates = query.getResultList();
        UahRate rate = rates.get(0);
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your bank ID first:");
        Long accNumber = Long.valueOf(sc.nextLine());
        try {
            client = findClient(accNumber);
            accList = client.getAccounts();
            System.out.println("""
                    What operation you want to make?
                    1 - UAH to EUR;
                    2 - UAH to USD;
                    3 - USD to UAH;
                    4 - EUR to UAH;
                    Please be sure you have respective amounts before you proceed!
                    """);
            String choice = sc.nextLine();
            switch (choice) {
                case "1":
                    if (!accList.isEmpty() &&
                            accList.stream().anyMatch(account -> account.getAcc_Currency().equals("UAH"))
                            && accList.stream().anyMatch(account -> account.getAcc_Currency().equals("EUR"))
                    ) {
                        System.out.println("Enter amount to exchange from UAH to EUR: ");
                        toBeConvertedAmount = Double.valueOf(sc.nextLine());
                        convertedAmount = toBeConvertedAmount / rate.getEurRate();
                        Long fromAccNum = null;
                        Long toAccNum = null;
                        for (Account a : accList) {
                            if (a.getAcc_Currency().equals("UAH")) {
                                fromAccNum = a.getAcc_Number();
                            }
                            if (a.getAcc_Currency().equals("EUR")) {
                                toAccNum = a.getAcc_Number();
                            }
                        }
                        if (!(fromAccNum == null) && !(toAccNum == null)) {
                            BigDecimal convAmount = new BigDecimal(convertedAmount);
                            BigDecimal convertedAmtTwoDig = convAmount.setScale(2, RoundingMode.HALF_UP);
                            convertedAmount = convertedAmtTwoDig.doubleValue();
                            convertTransaction(fromAccNum, toBeConvertedAmount, toAccNum, convertedAmount);
                        } else {
                            System.out.println("Error!");

                        }

                    }
                    break;
                case "2":
                    if (
                            !accList.isEmpty()
                                    && accList.stream().anyMatch(account -> account.getAcc_Currency().equals("UAH"))
                                    && accList.stream().anyMatch(account -> account.getAcc_Currency().equals("USD"))
                    ) {
                        System.out.println("Enter amount to exchange from UAH to USD:");
                        toBeConvertedAmount = Double.valueOf(sc.nextLine());
                        convertedAmount = toBeConvertedAmount / rate.getUsdRate();
                        Long fromAccNum = null;
                        Long toAccNum = null;
                        for (Account a : accList
                        ) {
                            if (a.getAcc_Currency().equals("UAH")) {
                                fromAccNum = a.getAcc_Number();
                            }
                            if (a.getAcc_Currency().equals("USD")) {
                                toAccNum = a.getAcc_Number();
                            }
                        }
                        if (!(fromAccNum == null) && !(toAccNum == null)) {
                            BigDecimal convAmount = new BigDecimal(convertedAmount);
                            BigDecimal convertedAmtTwoDig = convAmount.setScale(2, RoundingMode.HALF_UP);
                            convertedAmount = convertedAmtTwoDig.doubleValue();
                            convertTransaction(fromAccNum, toBeConvertedAmount, toAccNum, convertedAmount);
                        } else {
                            System.out.println("Error!");
                        }
                    }
                    break;

                case "3":
                    if (
                            !accList.isEmpty()
                                    && accList.stream().anyMatch(account -> account.getAcc_Currency().equals("UAH"))
                                    && accList.stream().anyMatch(account -> account.getAcc_Currency().equals("USD"))
                    ) {
                        System.out.println("Enter amount to exchange from USD to UAH: ");
                        toBeConvertedAmount = Double.valueOf(sc.nextLine());
                        convertedAmount = toBeConvertedAmount / rate.getUsdRate();
                        Long fromAccNum = null;
                        Long toAccNum = null;
                        for (Account a : accList
                        ) {
                            if (a.getAcc_Currency().equals("USD")) {
                                fromAccNum = a.getAcc_Number();
                            }
                            if (a.getAcc_Currency().equals("UAH")) {
                                toAccNum = a.getAcc_Number();
                            }
                        }
                        if (!(fromAccNum == null) && !(toAccNum == null)) {
                            BigDecimal convAmount = new BigDecimal(convertedAmount);
                            BigDecimal convertedAmtTwoDig = convAmount.setScale(2, RoundingMode.HALF_UP);
                            convertedAmount = convertedAmtTwoDig.doubleValue();
                            convertTransaction(fromAccNum, toBeConvertedAmount, toAccNum, convertedAmount);
                        } else {
                            System.out.println("Error!");
                        }
                    }
                    break;
                case "4":
                    if (
                            !accList.isEmpty()
                                    && accList.stream().anyMatch(account -> account.getAcc_Currency().equals("UAH"))
                                    && accList.stream().anyMatch(account -> account.getAcc_Currency().equals("EUR"))
                    ) {
                        System.out.println("Enter amount to exchange from EUR to UAH:");
                        toBeConvertedAmount = Double.valueOf(sc.nextLine());
                        convertedAmount = toBeConvertedAmount * rate.getEurRate();
                        Long fromAccNum = null;
                        Long toAccNum = null;
                        for (Account a : accList
                        ) {
                            if (a.getAcc_Currency().equals("EUR")) {
                                fromAccNum = a.getAcc_Number();
                            }
                            if (a.getAcc_Currency().equals("UAH")) {
                                toAccNum = a.getAcc_Number();
                            }
                        }
                        if (!(fromAccNum == null) && !(toAccNum == null)) {
                            //отримуємо лише 2 цифри після коми
                            BigDecimal convAmount = new BigDecimal(convertedAmount);
                            BigDecimal convertedAmtTwoDig = convAmount.setScale(2, RoundingMode.HALF_UP);
                            convertedAmount = convertedAmtTwoDig.doubleValue();
                            convertTransaction(fromAccNum, toBeConvertedAmount, toAccNum, convertedAmount);
                        } else {
                            System.out.println("Error!");
                        }
                    }
                    break;
                default:
                    return;
            }
        } catch (Exception e) {
            System.out.println("No client found, try again. ");
        }
    }

    private static boolean convertTransaction(long fromAcc, double amountSell, long toAcc, double amountBuy) {
        boolean result = false;
        EntityManagerFactory emf5 = Persistence.createEntityManagerFactory("bank");
        EntityManager em2 = emf5.createEntityManager();
        try {
            Query query = em2.createQuery("from Account a where a.acc_Number=:acc_n")
                    .setParameter("acc_n", fromAcc);
            Account withdrawalAccount = (Account) query.getSingleResult();
            em2.getTransaction().begin();
            em2.lock(withdrawalAccount, LockModeType.PESSIMISTIC_WRITE);
            if (withdrawalAccount.getBalance() >= amountSell) {
                withdrawalAccount.setBalance(withdrawalAccount.getBalance() - amountSell);
                em2.persist(withdrawalAccount);
                em2.lock(withdrawalAccount, LockModeType.NONE);
                em2.getTransaction().commit();
                result = true;

            } else {
                System.out.println("Insufficient funds! Your balance is: " + withdrawalAccount.getBalance() + withdrawalAccount.getAcc_Currency());
            }
            if (result) {
                query = em2.createQuery("from Account a where a.acc_Number=:acc_n")
                        .setParameter("acc_n", toAcc);
                Account depositedAccount = (Account) query.getSingleResult();
                em2.getTransaction().begin();
                em2.lock(depositedAccount, LockModeType.PESSIMISTIC_WRITE);
                depositedAccount.setBalance(depositedAccount.getBalance() + amountBuy);
                em2.persist(depositedAccount);
                em2.lock(depositedAccount, LockModeType.NONE);
                em2.getTransaction().commit();
                System.out.println("\tExchange successful: ");
                System.out.println("\tYour new balance is: " + depositedAccount.getBalance() + depositedAccount.getAcc_Currency());


            }
        } catch (Exception e) {
            em2.getTransaction().rollback();
            System.out.println("Error");
        } finally {
            em2.close();
            emf5.close();
        }
        return result;
    }
}
