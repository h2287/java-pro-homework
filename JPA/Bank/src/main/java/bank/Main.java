package bank;

import java.util.Scanner;

import static bank.Utils.*;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("""
                Are you an existing client or new?
                Press N new;
                Press E if you're our client""");
        String choice = sc.nextLine().toUpperCase();
        switch (choice) {
            case "N":
                System.out.println("Adding new client...");
                addClient();
                break;
            case "E":
                while (true) {
                    System.out.println("""
                            Welcome!
                            What do you wish to do?
                            1 - set up new account;
                            2 - withdraw money;
                            3 - deposit money;
                            4 - transfer funds;
                            5 - exchange currency;
                            """);
                    String action = sc.nextLine().toUpperCase();
                    switch (action) {
                        case "1":
                            addAccount();
                            break;
                        case "2":
                            withdraw();
                            break;
                        case "3":
                            deposit();
                            break;
                        case "4":
                            transfer();
                            break;
                        case "5":
                            exchange();
                            break;
                        default:
                            System.out.println("Wrong input!");
                            return;
                    }
                }
            default:
                System.out.println("Wrong input!");
        }
    }
}
