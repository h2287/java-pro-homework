package bank;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table
public class UahRate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(nullable = false)
private  double EurRate=39.00;

    @Column(nullable = false)
    private  double UsdRate=40.00;

    @Column(nullable = false)
    private LocalDate date;

    public UahRate(){
        this.date=LocalDate.now();
    }

    public double getEurRate() {
        return EurRate;
    }

    public void setEurRate(double eurRate) {
        EurRate = eurRate;
    }

    public double getUsdRate() {
        return UsdRate;
    }

    public void setUsdRate(double usdRate) {
        UsdRate = usdRate;
    }
}
